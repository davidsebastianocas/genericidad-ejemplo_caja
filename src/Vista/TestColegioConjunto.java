/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.Colegio;
import Negocio.ColegioConjunto;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MADARME
 */
public class TestColegioConjunto {
    
    public static void main(String[] args) {
        
        
        
        try {
            
            //ColegioConjunto myColegio=new ColegioConjunto("UFPS-teen",-4);
            ColegioConjunto myColegio=new ColegioConjunto("UFPS-teen",5);
            //Matricular:
            
            Estudiante e1=new Estudiante(1, "marcelo");
            Estudiante e2=new Estudiante(2, "agachate");
            Estudiante e3=new Estudiante(3, "y");
            Estudiante e4=new Estudiante(4, "conocelo");
            Estudiante e5=new Estudiante(5, "conocelo2");
            
            myColegio.matricular(e1);
            myColegio.matricular(e2);
            myColegio.matricular(e3);
            myColegio.matricular(e4);
            myColegio.matricular(e5);
            myColegio.desMatricular(e3);
            
            //Esto es un error:
            //myColegio.matricular(e4);
            
            System.out.println(myColegio.toString());
            System.out.println(myColegio.getListadoNombres());
            
            System.out.println("El estudiante "+e3.toString()+", esta en la posición:"+myColegio.getDondeEsta(e3));
            
            //Esto hay que implementarlo:
            
            myColegio.desMatricular(e5);
            System.out.println(myColegio.getListadoNombres());
            
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
    
}
