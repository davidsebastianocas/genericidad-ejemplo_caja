
package Vista;

import Util.Conjunto;
import javax.swing.JOptionPane;


public class TestNuevoConjunto {
    
    Conjunto<Integer> c1 = new Conjunto(8);
    Conjunto<Integer> c2 = new Conjunto(7);
    
    public static void main(String[] args) 
    {  
       
        
        TestNuevoConjunto prueba = new TestNuevoConjunto();
        
        prueba.llenarDatos();
        prueba.testRemoverDatos();
        prueba.testOrdenar();
        prueba.testConcatenar();
    }
    
    public void llenarDatos()
    {
        for (int i = 0; i < c1.getLength(); i++) 
            c1.adicionarElemento(Integer.parseInt(JOptionPane.showInputDialog("Conjunto 1("+(i+1)+"): Digite un numero")));   
            
        for (int i = 0; i < c2.getLength(); i++) 
            c2.adicionarElemento(Integer.parseInt(JOptionPane.showInputDialog("Conjunto 2("+(i+1)+"): Digite un numero"))); 
    }
    
    public void testRemoverDatos()
    {
        int preg;
        preg = Integer.parseInt(JOptionPane.showInputDialog("Desea remover algun numero del conjunto 1?\n1.Si\n2.No"));
        if (preg == 1){
            c1.remover(Integer.parseInt(JOptionPane.showInputDialog("Digite el numero a remover")));
            System.out.println("Nuevo Conjunto 1");
            System.out.println(""+c1.toString()+"");  
        }
        preg = Integer.parseInt(JOptionPane.showInputDialog("Desea remover algun numero del conjunto 2?\n1.Si\n2.No"));
        if (preg == 1){
            c2.remover(Integer.parseInt(JOptionPane.showInputDialog("Digite el numero a remover")));
            System.out.println("Nuevo Conjunto 2");
            System.out.println(""+c2.toString()+"");
        }  
    }
    
    public void testOrdenar()
    {
        int preg;
        preg = Integer.parseInt(JOptionPane.showInputDialog("Por que metodo desea ordenar el conjunto 1\n1.Ordenar por insercion\n2.Ordenar por Burbuja"));
        if (preg == 1)
        {
            c1.ordenar();
            System.out.println("conjunto 1: Ordenado Por Interseccion");
            System.out.println(""+c1.toString()+"");
        }
        if (preg == 2)
        {
            c1.ordenarBurbuja();
            System.out.println("Conjunto 1: Ordenado Por Burbuja");
            System.out.println(""+c1.toString()+"");
        }   
        
        preg = Integer.parseInt(JOptionPane.showInputDialog("Por que metodo desea ordenar el conjunto 2\n1.Ordenar por insercion\n2.Ordenar por Burbuja"));
        if (preg == 1)
        {
            c2.ordenar();
            System.out.println("conjunto 2: Ordenado Por Interseccion");
            System.out.println(""+c2.toString()+"");
        }
        if (preg == 2)
        {
            c2.ordenarBurbuja();
            System.out.println("Conjunto 2: Ordenado Por Burbuja");
            System.out.println(""+c2.toString()+"");
        }   
    }
    
    public void testConcatenar()
    {
        int preg;
        preg = Integer.parseInt(JOptionPane.showInputDialog("Por que metodo desea concatenar\n1.Normal\n2.Restrictivo"));
        if(preg == 1)
        {
            c1.concatenar(c2);
        
            System.out.println("Conjunto 1 concatenado con el conjunto 2");
            System.out.println(""+c1.toString()+"");
        }
        if(preg == 2)
        {
            c1.concatenarRestrictivo(c2);
        
            System.out.println("Conjunto 1 concatenado restrictivamente con el conjunto 2");
            System.out.println(""+c1.toString()+"");
        }
    }
}
